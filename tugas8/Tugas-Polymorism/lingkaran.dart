import 'bangun_datar.dart';

class Lingkaran extends BangunDatar {
  late double radius;

  Lingkaran(double radius) {
    this.radius = radius;
  }

  @override
  double luas() {
    return 3.14 * radius * radius;
  }

  @override
  double keliling() {
    return 2 * 3.14 * radius;
  }
}
