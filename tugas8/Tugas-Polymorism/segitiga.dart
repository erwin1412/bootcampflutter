import 'bangun_datar.dart';

class Segitiga extends BangunDatar {
  late double a;
  late double t;
  late double b;

  Segitiga (double a, double t, double b){
    this.a = a;
    this.t = t;
    this.b = b;
  }

  @override
  double luas(){
    return 0.5 * a * t;
  }

  @override
  double keliling(){
    return a + b + t;
  }
}