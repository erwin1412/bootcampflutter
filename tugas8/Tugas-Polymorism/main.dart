import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  BangunDatar bangunDatar = new BangunDatar();
  Lingkaran lingkaran = new Lingkaran(5);
  Persegi persegi = new Persegi(4);
  Segitiga segitiga = new Segitiga(3, 4, 5);

  bangunDatar.luas();
  bangunDatar.keliling();

  print("Luas lingkaran: ${lingkaran.luas()}");
  print("Keliling lingkaran: ${lingkaran.keliling()}");
  print("Luas persegi: ${persegi.luas()}");
  print("Keliling persegi: ${persegi.keliling()}");
  print("Luas segitiga: ${segitiga.luas()}");
  print("Keliling segitiga: ${segitiga.keliling()}");
}
