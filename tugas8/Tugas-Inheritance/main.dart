import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  armor_titan ar = armor_titan();
  attack_titan at = attack_titan();
  beast_titan b = beast_titan();
  human h = human();

  ar.powerPoint = 8;
  at.powerPoint = 7;
  b.powerPoint = 9;
  h.powerPoint = 4;

  print("Power Point Armor TItan : ${ar.powerPoint}");
  print("Power Point Attack TItan : ${at.powerPoint}");
  print("Power Point Beast TItan : ${b.powerPoint}");
  print("Power Point Human : ${h.powerPoint}");

  print("Sifat dari Armor Titan: "+ ar.terjang());
  print("Sifat dari Attack Titan: "+ at.punch());
  print("Sifat dari Beast Titan: "+ b.lempar());
  print("Sifat dari Human: "+ h.killAllTitan());
}