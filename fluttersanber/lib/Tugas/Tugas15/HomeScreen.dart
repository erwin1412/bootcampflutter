/*
import 'package:flutter/material.dart';
import 'package:fluttersanber/Tugas/Tugas12/DrawerScreen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          )
        ],
      ),
      drawer: DrawerScreen(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 10),
            //Icon notif dan shoppng chart
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(onPressed: () {}, icon: Icon(Icons.notifications)),
                IconButton(onPressed: () {}, icon: Icon(Icons.shopping_cart)),
              ],
            ),
            SizedBox(height: 37),
            // teks welcome, ......
            Text.rich(
              TextSpan(
                  text: "Welcome,",
                  style: TextStyle(fontWeight: FontWeight.bold),
                  children: [
                    TextSpan(
                        text: "\nPinaww",
                        style: TextStyle(fontWeight: FontWeight.normal)),
                  ]),
              style: TextStyle(fontSize: 50),
            ),
            SizedBox(height: 30),
            //search bar
            TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search, size: 18),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                hintText: "Search",
              ),
            ),
            SizedBox(height: 80),
            Text(
              "Saved Places",
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
            ),
            SizedBox(height: 10),
            //image
            SizedBox(
              height: 292.6,
              child: GridView.count(
                padding: EdgeInsets.zero,
                crossAxisCount: 2,
                childAspectRatio: 1.491,
                children: [
                  for (var country in countries)
                    Image.asset('assets/img/$country.png')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final countries = {'Monas', 'Roma', 'Berlin', 'Tokyo'};
*/