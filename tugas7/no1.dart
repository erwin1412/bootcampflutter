class Segitiga {
  double alas;
  double tinggi;
  double setengah;
  Segitiga(this.alas, this.tinggi, this.setengah);

  void info() {
    print("Segitiga");
  }

  double luas() {
    return (alas * tinggi * setengah);
  }
}

void main() {
  Segitiga s3 = Segitiga(20.0, 30.0, 0.5);
  s3.info();
  print(s3.luas());
}
