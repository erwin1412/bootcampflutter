import 'dart:io';
import 'lingkaran.dart';

void main(List<String> args) {
  lingkaran bundar;
  double luasBundar;

  bundar = new lingkaran();
  bundar.setRadius(-2);

  luasBundar = bundar.hitungLuas();

  stdout.write("$luasBundar \n");
}
