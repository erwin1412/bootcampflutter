class lingkaran {
  late double _radius;

  void setRadius(double value) {
    if (value < 0) {
      value *= -1;
    }
    _radius = value;
  }

  double getRadius() {
    return _radius;
  }

  double hitungLuas() {
    return 3.14 * _radius * _radius;
  }
}
