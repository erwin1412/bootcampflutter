void main(List<String> args) {
  var input = [
    ["0001", "Roman Almasya", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Simbring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Sanjaya", "Senjaya", "Martapura", "Berkebun"]
  ];

  for (var db in input) {
    print("Nama ID: ${db[0]}");
    print("Nama Lengkap : ${db[1]}");
    print("TTL  ${db[2]}");
    print("Hobi ${db[3]}");
  }
}
