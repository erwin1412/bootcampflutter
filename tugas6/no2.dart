void main(List<String> args) {
  print(rangeWithStep(1, 10, 2));
  print(rangeWithStep(11, 23, 3));
}

rangeWithStep(num1, num2, num3) {
  List<int> RangeList = [];
  if (num1 < num2) {
    for (int i = num1; i < num2; i++) {
      if (i == num1) {
        RangeList.add(i);
      } else {
        i = (num3 - 1) + i;
        RangeList.add(i);
      }
      for (int i = num1; i > num2; i--) {
        if (i == num1) {
          RangeList.add(i);
        } else {
          i = (num3 - 1) + i;
          RangeList.add(i);
        }
      }
    }
  }
  return (RangeList);
}
