import 'dart:io';

void main(List<String> args) {
  for (var i = 1; i <= 40; i++) {
    if (i % 8 == 0) {
      stdout.write("#\n");
    } else {
      stdout.write("#");
    }
  }
}

/*
########
########
########
######## 
*/
