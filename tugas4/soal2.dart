void main(List<String> args) {
  for (var i = 1; i < 20; i++) {
    if (i % 3 == 1) {
      print("$i - Santai");
    } else if (i % 3 == 2) {
      print("$i - Berkualitas");
    } else {
      print("$i - I Love Coding");
    }
  }
}
/*SYARAT:
A. Jika angka ganjil maka tampilkan Santai
B. Jika angka genap maka tampilkan Berkualitas
C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
 */