void main(List<String> args) {
  var i = 1;
  var n = 20;
  print("LOOPING PERTAMA");
  while (i <= n) {
    if (i % 2 == 0) {
      print("$i - I love coding");
    }
    i++;
  }
  var b = 1;
  var c = 20;
  print("LOOPING KEDUA");
  while (c >= b) {
    if (c % 2 == 0) {
      print("$c - I will become a mobile developer");
    }
    c = c - 2;
  }
}
