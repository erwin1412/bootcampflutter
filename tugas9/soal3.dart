void main(List<String> args) async {
  print("Ready. Sing");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async {
  String a = "pernahkan kau merasa";
  return await Future.delayed(Duration(seconds: 5), () => (a));
}

Future<String> line2() async {
  String b = "pernahkan kau merasa.....";
  return await Future.delayed(Duration(seconds: 3), () => (b));
}

Future<String> line3() async {
  String c = "pernahkan kau merasa";
  return await Future.delayed(Duration(seconds: 2), () => (c));
}

Future<String> line4() async {
  String d = "Hatimu hampa, pernahkan kau merasa hati mu kosong....";
  return await Future.delayed(Duration(seconds: 1), () => (d));
}
