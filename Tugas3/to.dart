import 'dart:io';

void main(List<String> args) {
  print("apakah anda ingin menginstall aplikasi? Y/T");
  String? jawab = stdin.readLineSync();

  if ((jawab == "Y") || (jawab == "y")) {
    print("anda akan menginstall aplikasi dart");
  } else if ((jawab == "T") || (jawab == "T")) {
    print("aborted");
  } else {
    while ((jawab != "Y" && jawab != "y" && jawab != "t" && jawab != "T")) {
      print("jawaban kamu *$jawab* silakan jawab Y/T");
      jawab = stdin.readLineSync();
    }
  }
}

/*1. Ternary operator 
Petunjuk : untuk membuat sebuah kondisi ternary dimna anda akan di minta untuk menginstall aplikasi 
dengan jawaban Y/T , jadi tugas teman 
teman sekrang adalah memberi jawaban y/t saat ada input mau
 diinstall aplikasi, apabila 
 ( y )maka akan menampilkan "anda akan menginstall aplikasi dart", 
 jika (T)  maka akan keluar pesan "aborted" (Gunakan I/O).*/