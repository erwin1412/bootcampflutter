void main(List<String> args) {
  var hari = 1;
  var bulan = 5;
  var tahun = 1945;

  switch (bulan) {
    case 1:
      {
        print('$hari Januari $tahun');
        break;
      }
    case 2:
      {
        print('$hari Februari $tahun');
        break;
      }
    case 3:
      {
        print('$hari Maret $tahun');
        break;
      }
    case 4:
      {
        print('$hari April $tahun');
        break;
      }
    case 5:
      {
        print('$hari Mei $tahun');
        break;
      }
    case 6:
      {
        print('$hari Juni $tahun');
        break;
      }
    case 7:
      {
        print('$hari Juli $tahun');
        break;
      }
    case 8:
      {
        print('$hari Agustus $tahun');
        break;
      }
    case 9:
      {
        print('$hari September $tahun');
        break;
      }
    case 10:
      {
        print('$hari Oktober $tahun');
        break;
      }
    case 11:
      {
        print('$hari November $tahun');
        break;
      }
    case 12:
      {
        print('$hari Desember $tahun');
        break;
      }
  }
}
/*4. Switch Case
Kamu akan diberikan sebuah data dalam tiga variabel, yaitu hari, bulan, dan tahun. 
Disini kamu diminta untuk membuat format tanggal. Misal tanggal yang diberikan adalah 
hari 1, bulan 5, dan tahun 1945. Maka, output yang harus kamu proses adalah menjadi 1 Mei 1945.

Gunakan switch case untuk kasus ini, tidak perlu menggunakna i/o!

Contoh:

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
Skeleton Code / Code yang dicontohkan yang perlu diikuti dan dimodifikasi

var tanggal; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31) var bulan;

// assign nilai variabel bulan disini! (dengan angka antara 1 - 12) var tahun;

// assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

 */