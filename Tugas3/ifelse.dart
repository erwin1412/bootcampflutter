import 'dart:io';

void main(List<String> args) {
  print("selamat datang di dunia game werewolf");
  print("silahkan isi nama anda");
  String? nama = stdin.readLineSync();
  while (nama == "") {
    print("Nama harus diisi!");
    nama = stdin.readLineSync();
  }
  print("nama anda = $nama");

  print("Pilih Peranmu untuk memulai game");
  print("silahkan input angka 1 / 2 / 3 / 4");
  print("1.manusia");
  print("2.Penyihir");
  print("3.guardian");
  print("4.Werewolf");
  String? role = stdin.readLineSync();
  while (role == "") {
    print("role harus diisi!");
    role = stdin.readLineSync();
  }
  if (role == "1") {
    role = "manusia";
  } else if (role == "2") {
    role = "Seer";
  } else if (role == "3") {
    role = "guardian";
  } else {
    role = "werewolf";
  }
  print("role anda = $role");
  print("game dimulai");
}
/*2. If-else if dan else
Petunjuk : Kita akan memasuki dunia game werewolf. 
Pada saat akan bermain kamu diminta memasukkan nama dan peran . 
Untuk memulai game pemain harus memasukkan variable nama dan peran. 
Jika pemain tidak memasukkan nama maka game akan mengeluarkan warning 
“Nama harus diisi!“. 
Jika pemain memasukkan nama tapi tidak memasukkan peran maka game akan mengeluarkan warning 
“Pilih Peranmu untuk memulai game“. Terdapat tiga peran yaitu penyihir, guard, dan werewolf. 
Tugas kamu adalah membuat program untuk mengecek input dari pemain dan hasil response dari game sesuai 
input yang dikirimkan.

Petunjuk:

Nama dan peran diisi dengan i/o dan bisa diisi apa saja
Nama perlu dicek persis sesuai dengan input/output
Buat kondisi if-else untuk masing-masing peran
Input:

var nama = "John"
var peran = ""
Output:

// Output untuk Input nama = '' dan peran = '' "Nama harus diisi!" //Output untuk Input nama = 'John' dan peran = '' "Halo John, Pilih peranmu untuk memulai game!" //Output untuk Input nama = 'Jane' dan peran 'Penyihir' "Selamat datang di Dunia Werewolf, Jane" "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!" //Output untuk Input nama = 'Jenita' dan peran 'Guard' "Selamat datang di Dunia Werewolf, Jenita" "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf." //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf' "Selamat datang di Dunia Werewolf, Junaedi" "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"*/