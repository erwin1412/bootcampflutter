import 'dart:io';

void main() {
  stdout.write("Silahkan input angka = ");
  String? angka = stdin.readLineSync();
  var a = int.parse(angka!);
  stdout.write("Hasil Faktorial = ");
  print(factorial(a));
}

factorial(number) {
  if (number <= 0) {
    // termination case
    return 1;
  } else {
    return (number * factorial(number - 1));
    // function invokes itself
  }
}
